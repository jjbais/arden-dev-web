// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyADLVRXJDRcCKKXUzTMgi79gJUxua_DJks",
    authDomain: "arden-dev.firebaseapp.com",
    databaseURL: "https://arden-dev.firebaseio.com",
    projectId: "arden-dev",
    storageBucket: "arden-dev.appspot.com",
    messagingSenderId: "65249844326",
    appId: "1:65249844326:web:94e97ba7437aa63b8d61a7",
    measurementId: "G-YWTZDHYPBS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
