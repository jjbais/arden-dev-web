import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ClassDirective } from '@angular/flex-layout';

class Car{
  id: String;
  make: string;
  max_speed: number;
  engine_size: number;
  created_date: Date;

}

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss'],
})
export class WelcomeComponent implements OnInit {
  cars: Car[];

  constructor(public afFirestore: AngularFirestore) {}

  ngOnInit(): void {
    this.afFirestore.collection('cars').snapshotChanges().subscribe((data) => {
      this.cars = data.map((e) => {
        return {
          id: e.payload.doc.id,
          make: e.payload.doc.data(),
          max_speed: e.payload.doc.data(),
          engine_size: e.payload.doc.data(),
          created_date: e.payload.doc.data()
        } as Car;
      });
    });
  }
}
